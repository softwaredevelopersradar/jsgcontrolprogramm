package ValuesCorrectLib.Events;


import KvetkaModels.NameTable;

public interface ITableEvents {

    public void OnAddRecord(TableEvents tableModel);
    public void OnChangeRecord(TableEvents tableModel);
    public void OnDeleteRecord(TableEvents tableModel);
    public void OnClearRecords(NameTable nameTable);

    public void OnSelectedStation(int id);
}
