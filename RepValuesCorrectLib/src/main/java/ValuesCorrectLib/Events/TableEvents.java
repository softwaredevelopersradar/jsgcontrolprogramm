package ValuesCorrectLib.Events;


import KvetkaModels.AbstractCommonTable;
import KvetkaModels.InfoTable;
import KvetkaModels.NameTable;

public class TableEvents {

    private AbstractCommonTable record;
    private NameTable nameTable;
    private Integer id;

    public TableEvents(AbstractCommonTable record)
    {
        this.record = record;
        this.nameTable =  record.getClass().getAnnotation(InfoTable.class).name();
        this.id = record.id;
    }

    public AbstractCommonTable getRecord() { return record; }

    public NameTable getNameTable() { return nameTable; }
    public void setNameTable(NameTable nameTable) { this.nameTable = nameTable; }

    public Integer getId() { return id; }
}
