package ValuesCorrectLib;

import KvetkaModels.Bandwidth;
import KvetkaModels.CommunicationType;
import KvetkaModels.Led;
import ValuesCorrectLib.Translation.CMeaning;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ValuesCorrectLib.Translation.CTypeModulation;

public class Converters {

    public static String RoleConverter(byte role)
    {
        String strRole = "";

        switch (role)
        {
            case 0: strRole = "Автономная"; break;
            case 1: strRole = "Ведущая"; break;
            case 2: strRole = "Ведомая"; break;
        }

        return strRole;
    }

    public static byte RoleConverter(String role)
    {
        byte bRole = -1;

        switch (role)
        {
            case "Автономная": bRole = 0; break;
            case "Ведущая": bRole = 1; break;
            case "Ведомая": bRole = 2; break;
        }

        return bRole;
    }

    public static String CommunicationTypeConverter(CommunicationType communicationType)
    {
        String strCommunicationType = "";

        switch (communicationType)
        {
            case Modem4Wire: strCommunicationType = CommunicationType.Modem4Wire.getCommunicationName(); break;
            case RadioModem: strCommunicationType = CommunicationType.RadioModem.getCommunicationName(); break;
            case Router3G_4G: strCommunicationType = CommunicationType.Router3G_4G.getCommunicationName(); break;
        }

        return strCommunicationType;
    }

    public static String LongitudeE_WConverter(double longitude)
    {
        if (longitude < 0)
            return "W ";
        else return "E ";
    }

    public static String LatitudeS_NConverter(double latitude)
    {
        if (latitude < 0)
            return "S ";
        else return "N ";
    }

    public static Bandwidth BandwidthConverter(String bandwidth) {

        switch (bandwidth) {

            case "4000":
                return Bandwidth.B4000;

            case "8000":
                return Bandwidth.B8000;

            case "21000":
                return Bandwidth.B21000;
        }
        return Bandwidth.B8000;
    }

    /**
     * декодирование параметров помехи из кодов в строку
     * @param bModulation модуляция
     * @param bDeviation девиация
     * @param bManipulation манипуляция
     * @param bDuration длительность
     * @return
     */
    public static String StrInterferenceParams(byte bModulation, byte bDeviation, byte bManipulation, byte bDuration)
    {
        String strModulation = "";    // модуляция
        String strSpectrumWidth = ""; // ширина спектра
        String strDuration = "";      // длительность

        String strInterferenceParams = "";    // все параметры

        switch (bModulation) {

            case 1:
                strModulation = CTypeModulation.paramChMSh; // "ЧМШ";

                switch (bDeviation)
                {
                    case 1:
                        strSpectrumWidth = "0.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = "1 " + CMeaning.meaningkHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = "1.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = "2 " + CMeaning.meaningkHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = "3.4 " + CMeaning.meaningkHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = "4 " + CMeaning.meaningkHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = "8 " + CMeaning.meaningkHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = "16 " + CMeaning.meaningkHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 2:
                strModulation = CTypeModulation.paramChMShScan; // "ЧМШ скан. f";

                switch (bDeviation)
                {
                    case 1:
                        strSpectrumWidth = "0.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = "1 " + CMeaning.meaningkHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = "1.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = "2 " + CMeaning.meaningkHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = "3.4 " + CMeaning.meaningkHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = "4 " + CMeaning.meaningkHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = "8 " + CMeaning.meaningkHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = "16 " + CMeaning.meaningkHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 3:
                strModulation = CTypeModulation.paramChM2; // "ЧМ-2";

                switch (bManipulation)
                {
                    case 1:
                        strSpectrumWidth = String.valueOf(bDeviation * 20 + 5) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = String.valueOf(bDeviation * 20 + 10) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = String.valueOf(bDeviation * 20 + 20) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = String.valueOf(bDeviation * 20 + 50) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = String.valueOf(bDeviation * 20 + 100) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = String.valueOf(bDeviation * 20 + 200) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = String.valueOf(bDeviation * 20 + 500) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = String.valueOf(bDeviation * 20 + 1000) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 9:
                        strSpectrumWidth = String.valueOf(bDeviation * 20 + 2000) + " " + CMeaning.meaningHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 4:
                strModulation = CTypeModulation.paramChM4; // "ЧМ-4";

                switch (bManipulation)
                {
                    case 1:
                        strSpectrumWidth = String.valueOf(3 * bDeviation * 20 + 5) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = String.valueOf(3 * bDeviation * 20 + 10) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = String.valueOf(3 * bDeviation * 20 + 20) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = String.valueOf(3 * bDeviation * 20 + 50) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = String.valueOf(3 * bDeviation * 20 + 100) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = String.valueOf(3 * bDeviation * 20 + 200) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = String.valueOf(3 * bDeviation * 20 + 500) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = String.valueOf(3 * bDeviation * 20 + 1000) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 9:
                        strSpectrumWidth = String.valueOf(3 * bDeviation * 20 + 2000) + " " + CMeaning.meaningHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 5:
                strModulation = CTypeModulation.paramChM8; // "ЧМ-8";

                switch (bManipulation)
                {
                    case 1:
                        strSpectrumWidth = String.valueOf(7 * bDeviation * 20 + 5) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = String.valueOf(7 * bDeviation * 20 + 10) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = String.valueOf(7 * bDeviation * 20 + 20) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = String.valueOf(7 * bDeviation * 20 + 50) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = String.valueOf(7 * bDeviation * 20 + 100) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = String.valueOf(7 * bDeviation * 20 + 200) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = String.valueOf(7 * bDeviation * 20 + 500) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = String.valueOf(7 * bDeviation * 20 + 1000) + " " + CMeaning.meaningHz + " ";
                        break;

                    case 9:
                        strSpectrumWidth = String.valueOf(7 * bDeviation * 20 + 2000) + " " + CMeaning.meaningHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 6:
                strModulation = CTypeModulation.paramFM2; // "ФМ-2";

                switch (bDeviation)
                {
                    case 1:
                        strSpectrumWidth = "100 " + CMeaning.meaningkHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = "50 " + CMeaning.meaningkHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = "20 " + CMeaning.meaningkHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = "10 " + CMeaning.meaningkHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = "5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = "2 " + CMeaning.meaningkHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = "1 " + CMeaning.meaningkHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = "0.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 9:
                        strSpectrumWidth = "0.2 " + CMeaning.meaningkHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 7:
                strModulation = CTypeModulation.paramFM4; // "ФМ-4";

                switch (bDeviation)
                {
                    case 1:
                        strSpectrumWidth = "100 " + CMeaning.meaningkHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = "50 " + CMeaning.meaningkHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = "20 " + CMeaning.meaningkHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = "10 " + CMeaning.meaningkHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = "5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = "2 " + CMeaning.meaningkHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = "1 " + CMeaning.meaningkHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = "0.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 9:
                        strSpectrumWidth = "0.2 " + CMeaning.meaningkHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 8:
                strModulation = CTypeModulation.paramFM8; // "ФМ-8";

                switch (bDeviation)
                {
                    case 1:
                        strSpectrumWidth = "100 " + CMeaning.meaningkHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = "50 " + CMeaning.meaningkHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = "20 " + CMeaning.meaningkHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = "10 " + CMeaning.meaningkHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = "5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = "2 " + CMeaning.meaningkHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = "1 " + CMeaning.meaningkHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = "0.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 9:
                        strSpectrumWidth = "0.2 " + CMeaning.meaningkHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 9:
                strModulation = CTypeModulation.paramZSh; // "ЗШ";

                switch (bDeviation)
                {
                    case 1:
                        strSpectrumWidth = "2500 " + CMeaning.meaningkHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = "1000 " + CMeaning.meaningkHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = "500 " + CMeaning.meaningkHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = "200 " + CMeaning.meaningkHz + " ";
                        break;

                   default:
                        break;
                }
                break;

            case 0x0C:
                strModulation = CTypeModulation.paramAFMn; // "АФМн";

                switch (bDeviation)
                {
                    case 1:
                        strSpectrumWidth = "100 " + CMeaning.meaningkHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = "50 " + CMeaning.meaningkHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = "20 " + CMeaning.meaningkHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = "10 " + CMeaning.meaningkHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = "5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = "2 " + CMeaning.meaningkHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = "1 " + CMeaning.meaningkHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = "0.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 9:
                        strSpectrumWidth = "0.2 " + CMeaning.meaningkHz + " ";
                        break;

                    default:
                        break;
                }
                break;
        }

        switch (bDuration) {

            case 1:
                strDuration = "0.5 " + CMeaning.meaningms + " ";
                break;

            case 2:
                strDuration = "1 " + CMeaning.meaningms + " ";
                break;

            case 3:
                strDuration = "1.5 " + CMeaning.meaningms + " ";
                break;

            case 4:
                strDuration = "2 " + CMeaning.meaningms + " ";
                break;

            default:
                strDuration = "";
                break;
        }

        strInterferenceParams = strModulation + " " + strSpectrumWidth + " " + strDuration;

        return strInterferenceParams;
    }

    public static ImageView LedConverter(Led led)
    {
        if (led == null) return new ImageView();

        ImageView imageView = new ImageView();
        imageView.setFitHeight(15);
        imageView.setFitWidth(15);

        switch (led){

            case Empty:

                imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/empty.png")));
                break;

            case Gray:

                imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/gray.png")));
                break;

            case Green:

                imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/green.png")));
                break;

            case Red:

                imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/red.png")));
                break;

            case Blue:

                imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/blue.png")));
                break;

            case Yellow:

                imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/yellow.png")));
                break;

            default:
                break;
        }

        return imageView;
    }
}
