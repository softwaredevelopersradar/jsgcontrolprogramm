package ValuesCorrectLib.Suppress;

import KvetkaModels.SpecFreqsModel;
import KvetkaModels.SuppressFWSModel;

import java.util.List;

public class CorrectParamsSuppressFWS {

    /**
     * Принадлежность частоты заданным диапазонам РП
     * @param dFreq частота
     * @param specFreqsModel проверяемые диапазоны
     * @return сообщение об ошибке (пустая строка - успешно)
     */
    public static String IsCheckRangesSuppress(double dFreq, List<SpecFreqsModel> specFreqsModel)
    {
        String sMessage = "Частота не принадлежит диапазону РП!";

        long len = specFreqsModel.stream().count();
        for (int i = 0; i < len; i++) {

            if (dFreq >= specFreqsModel.get(i).getFreqMin() && dFreq <= specFreqsModel.get(i).getFreqMax() && specFreqsModel.get(i).getIsCheck())
                sMessage = "";
        }
        return sMessage;
    }

    /**
     * Принадлежность частоты Запрещенным частотам
     * @param dFreq частота
     * @param freqsForbidden проверяемые диапазоны
     * @return сообщение об ошибке (пустая строка - успешно)
     */
    public static String IsCheckForbiddenFreq(double dFreq, List<SpecFreqsModel> freqsForbidden)
    {
        String sMessage = "";

        long len = freqsForbidden.stream().count();
        for (int i = 0; i < len; i++) {

            if (dFreq >= freqsForbidden.get(i).getFreqMin() && dFreq <= freqsForbidden.get(i).getFreqMax())
                sMessage = "Частота не может быть добавлена, так как запрещенная!";
        }
        return sMessage;
    }

    /**
     * Количество ИРИ в данной литере
     * @param bLetter литера
     * @param countIRI количество ИРИ
     * @param suppressFWSModel
     * @return
     */
    public static String CountIRILetter(byte bLetter, int countIRI, List<SuppressFWSModel> suppressFWSModel)
    {
        String sMessage = "";

        long iCount = suppressFWSModel.stream().filter(x -> x.getLetter() == bLetter).count();

        if (iCount == countIRI) {

            sMessage = "Количество ИРИ в данной литере равно максимальному значению!";
        }
        return sMessage;
    }
}
