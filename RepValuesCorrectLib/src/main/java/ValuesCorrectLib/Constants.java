package ValuesCorrectLib;

public class Constants {

    /**
     * Диапазоны литер
     */
    public static final double FREQ_START_LETTER_1 = 1500.0F;
    public static final double FREQ_START_LETTER_2 = 18000.0F;
    public static final double FREQ_STOP_LETTER_2 = 30000.0F;
}
