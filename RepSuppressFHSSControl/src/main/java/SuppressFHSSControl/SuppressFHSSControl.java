package SuppressFHSSControl;

import FPSModels.InterferenceParams;
import FPSModels.SuppressFHSSModel;
import FPSValuesLib.Converters;
import FPSValuesLib.GetParams;
import FPSValuesLib.PropertiesValues;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


public class SuppressFHSSControl extends AnchorPane implements IPropertySuppressFHSSEvents, Initializable {

    //region Controls, Containers
    @FXML public TableColumn<SuppressFHSSModel, Integer> idColumn;
    @FXML public TableColumn<SuppressFHSSModel, Integer> freqMinColumn;
    @FXML public TableColumn<SuppressFHSSModel, Integer> freqMaxColumn;
    @FXML public TableColumn<SuppressFHSSModel, InterferenceParams> interferenceParamsColumn;

    @FXML public TableView<SuppressFHSSModel> tableSuppressFHSS;
    // endregion

    Properties properties = new Properties();
    WndPropertySuppressFHSS wndProperty;

    private List<ITableSuppressFHSSEvents> listeners = new ArrayList();

    public void addListener(ITableSuppressFHSSEvents toAdd) {
        this.listeners.add(toAdd);
    }

    public SuppressFHSSControl() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SuppressFHSS.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            fxmlLoader.load();

            Callback<TableColumn<SuppressFHSSModel, Boolean>, TableCell<SuppressFHSSModel, Boolean>> cellFactory = (
                    TableColumn<SuppressFHSSModel, Boolean> p) -> new EditingCell();
        }
        catch (IOException exception) {

            throw new RuntimeException(exception);
        }
    }

    @FXML
    private void AddRecord_Click(ActionEvent event) {

        wndProperty = new WndPropertySuppressFHSS();
        wndProperty.modulationField.getSelectionModel().select(5);
        wndProperty.manipulationField.getSelectionModel().select(0);
        wndProperty.deviationField.getSelectionModel().select(0);
        wndProperty.durationField.getSelectionModel().select(0);

        PropertiesValues.setIndexModulation(Byte.parseByte(String.valueOf(wndProperty.modulationField.getSelectionModel().getSelectedIndex())));

        wndProperty.IsAddRec = true;

        wndProperty.addListenerProperty(this);

    }

    @FXML
    private void ChangeRecord_Click(ActionEvent event) {

        SuppressFHSSModel tableModel = tableSuppressFHSS.getSelectionModel().getSelectedItem();

        if(tableModel != null) {

            wndProperty = new WndPropertySuppressFHSS(tableModel);
            wndProperty.setIdField(tableModel.getId());
            wndProperty.freqMinField.getValueFactory().setValue(tableModel.getFreqMin());
            wndProperty.freqMaxField.getValueFactory().setValue(tableModel.getFreqMax());
            wndProperty.modulationField.getSelectionModel().select(PropertiesValues.getIndexModulation());
            wndProperty.manipulationField.getSelectionModel().select(PropertiesValues.getIndexManipulation());
            if(PropertiesValues.getIndexModulation() > 1 && PropertiesValues.getIndexModulation() < 5) {
                wndProperty.deviationField.getItems().clear();
                wndProperty.deviationField.getItems().add(String.valueOf(PropertiesValues.getValueDeviation()));
                wndProperty.deviationField.getSelectionModel().select(0);
            }
            else {
                wndProperty.deviationField.getSelectionModel().select(PropertiesValues.getIndexDeviation());
            }
            wndProperty.freqSpacingField.getValueFactory().setValue(PropertiesValues.getValueFreqSpacing());
            wndProperty.durationField.getSelectionModel().select(PropertiesValues.getIndexDuration());

            wndProperty.IsChangeRec = true;

            wndProperty.addListenerProperty(this);
        }
    }

    @FXML
    private void DeleteRecord_Click(ActionEvent event) {

        if(tableSuppressFHSS.getSelectionModel().isEmpty()) {
            return;
        }

        SuppressFHSSModel tableModel = tableSuppressFHSS.getSelectionModel().getSelectedItem();

        SuppressFHSSModel table = new SuppressFHSSModel();
        table.setId(tableModel.getId());
        table.setFreqMin(tableModel.getFreqMin());
        table.setFreqMax(tableModel.getFreqMax());
        table.setInterferenceParams(tableModel.getInterferenceParams());

    }

    @FXML
    private void ClearRecords_Click(ActionEvent event) {

        ObservableList<SuppressFHSSModel> templist = FXCollections.observableArrayList(new SuppressFHSSModel[]{});
        tableSuppressFHSS.setItems(templist);

    }

    /**
     * Обновить таблицу
     * @param list
     */
    public void UpdateSuppressFHSS(List<SuppressFHSSModel> list)
    {
        try
        {
            if (list == null)
                return;

            ObservableList<SuppressFHSSModel> tempList = FXCollections.observableArrayList(list);

            tableSuppressFHSS.setItems(tempList);

            SelectedRow(properties.getSelectedRow());
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void SelectedRow(int ind)
    {
        tableSuppressFHSS.getSelectionModel().clearSelection();
        tableSuppressFHSS.requestFocus();
        tableSuppressFHSS.getFocusModel().focus(ind);
        tableSuppressFHSS.getSelectionModel().select(ind);

        GetSelectedRowParams();
    }

    @FXML
    private void Mouse_Click(MouseEvent event) {

        properties.setSelectedRow(tableSuppressFHSS.getSelectionModel().getSelectedIndex());

        GetSelectedRowParams();
    }

    private void GetSelectedRowParams() {

        try {

            if(tableSuppressFHSS.getSelectionModel().getSelectedItem() == null) {
                return;
            }

            //modulation АФМн  0x0C hex = 12 dec
            int mod = tableSuppressFHSS.getSelectionModel().getSelectedItem().getInterferenceParams().modulation - 1;
            if(mod == 11) mod = 9;
            PropertiesValues.setIndexModulation((byte)mod);

            if(mod > 1 && mod < 5) {

                double dDev = GetParams.GetSpectrumWidth(tableSuppressFHSS.getSelectionModel().getSelectedItem().getInterferenceParams().modulation,
                        tableSuppressFHSS.getSelectionModel().getSelectedItem().getInterferenceParams().deviation,
                        tableSuppressFHSS.getSelectionModel().getSelectedItem().getInterferenceParams().manipulation);
                PropertiesValues.setValueDeviation(dDev);
            }
            else {

                int dev = tableSuppressFHSS.getSelectionModel().getSelectedItem().getInterferenceParams().deviation - 1;
                PropertiesValues.setIndexDeviation((byte)dev);
            }

            int man = tableSuppressFHSS.getSelectionModel().getSelectedItem().getInterferenceParams().manipulation - 1;
            PropertiesValues.setIndexManipulation((byte)man);

            int dur = tableSuppressFHSS.getSelectionModel().getSelectedItem().getInterferenceParams().duration - 1;
            PropertiesValues.setIndexDuration((byte)dur);

            PropertiesValues.setValueFreqSpacing(tableSuppressFHSS.getSelectionModel().getSelectedItem().getInterferenceParams().deviation * 20);
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Test Add records to list
     */
    public ObservableList<SuppressFHSSModel> AddRecToList() {

        // создаем список объектов
        ObservableList<SuppressFHSSModel> list = FXCollections.observableArrayList();


        return list;
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        idColumn.setCellValueFactory(new PropertyValueFactory<SuppressFHSSModel, Integer>("id"));
        freqMinColumn.setCellValueFactory(new PropertyValueFactory<SuppressFHSSModel, Integer>("freqMin"));
        freqMaxColumn.setCellValueFactory(new PropertyValueFactory<SuppressFHSSModel, Integer>("freqMax"));
        interferenceParamsColumn.setCellValueFactory(new PropertyValueFactory<SuppressFHSSModel, InterferenceParams>("interferenceParams"));


        tableSuppressFHSS.setPlaceholder(new Label("Записи в таблице отсутствуют!"));

        /**
         * Редактирование
         */
//        freqMinColumn.setCellFactory(new Callback<TableColumn<SuppressFHSSModel, Double>, TableCell<SuppressFHSSModel, Double>>() {
//
//            @Override
//            public TableCell<SuppressFHSSModel, Double> call(TableColumn<SuppressFHSSModel, Double> param) {
//                return new TableCell<SuppressFHSSModel, Double>(){
//
//                    private final Label label;
//                    {
//                        label = new Label();
//                        label.setStyle("-fx-text-fill: white;");
//                    }
//                    @Override
//                    protected void updateItem(Double value, boolean empty){
//                        super.updateItem(value, empty);
//                        if (empty){
//                            setGraphic(null);
//                        } else {
//                            label.setText(new DecimalFormat("#0.0").format(value));
//                            setGraphic(label);
//                        }
//                    }
//                };
//            }
//        });

//        freqMaxColumn.setCellFactory(new Callback<TableColumn<SuppressFHSSModel, Double>, TableCell<SuppressFHSSModel, Double>>() {
//
//            @Override
//            public TableCell<SuppressFHSSModel, Double> call(TableColumn<SuppressFHSSModel, Double> param) {
//                return new TableCell<SuppressFHSSModel, Double>(){
//
//                    private final Label label;
//                    {
//                        label = new Label();
//                        label.setStyle("-fx-text-fill: white;");
//                    }
//                    @Override
//                    protected void updateItem(Double value, boolean empty){
//                        super.updateItem(value, empty);
//                        if (empty){
//                            setGraphic(null);
//                        } else {
//                            label.setText(new DecimalFormat("#0.0").format(value));
//                            setGraphic(label);
//                        }
//                    }
//                };
//            }
//        });

        interferenceParamsColumn.setCellFactory(new Callback<TableColumn<SuppressFHSSModel, InterferenceParams>, TableCell<SuppressFHSSModel, InterferenceParams>>() {

            @Override
            public TableCell<SuppressFHSSModel, InterferenceParams> call(TableColumn<SuppressFHSSModel, InterferenceParams> param) {
                return new TableCell<SuppressFHSSModel, InterferenceParams>(){

                    private final Label label;
                    {
                        label = new Label();
                        label.setStyle("-fx-text-fill: white;");
                    }
                    @Override
                    protected void updateItem(InterferenceParams value, boolean empty){
                        super.updateItem(value, empty);
                        if (empty){
                            setGraphic(null);
                        } else {
                            String strInterferenceParams = Converters.StrInterferenceParams(value.modulation, value.deviation, value.manipulation, value.duration);
                            label.setText(strInterferenceParams);
                            setGraphic(label);
                        }
                    }
                };
            }
        });
   }

    @Override
    public void OnIsAddRecord(Boolean b) {

        if(b) {

            for (ITableSuppressFHSSEvents tableEvent : listeners) {
                tableEvent.OnAddRecord(new TableSuppressFHSSEvents(wndProperty.SuppressFHSSModel));
            }
        }
    }

    @Override
    public void OnIsChangeRecord(Boolean b) {

        if(b) {

            for (ITableSuppressFHSSEvents tableEvent : listeners) {

                tableEvent.OnChangeRecord(new TableSuppressFHSSEvents(wndProperty.SuppressFHSSModel));

            }
        }
    }
}

class EditingCell extends TableCell<SuppressFHSSModel, Boolean> {

    private Boolean textField;

    public EditingCell() {
    }

    @Override
    public void startEdit() {
        super.startEdit();

        if (textField == null) {
            createTextField();
        }
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText(String.valueOf(getItem()));
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    @Override
    public void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
        } else {
            if (isEditing()) {
                if (textField != null) {
//                    textField.setText(getString());
                }
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            } else {
                setContentDisplay(ContentDisplay.TEXT_ONLY);
            }
        }
    }

    private void createTextField() {
//        textField = new Boolean() TextField(getBoolean());
//        textField.setOnKeyPressed(t -> {
//            if (t.getCode() == KeyCode.ENTER) {
//                commitEdit(textField.getText());
//            } else if (t.getCode() == KeyCode.ESCAPE) {
//                cancelEdit();
//            }
//        });
    }

    private Boolean getBoolean() {
        return getItem() == true ? false : getItem();
    }

}