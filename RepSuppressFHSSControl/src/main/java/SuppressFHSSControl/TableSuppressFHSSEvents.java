package SuppressFHSSControl;

import FPSModels.SuppressFHSSModel;

public class TableSuppressFHSSEvents {
    private SuppressFHSSModel record;
    private Integer id;

    public TableSuppressFHSSEvents(SuppressFHSSModel record)
    {
        this.record = record;
        this.id = record.getId();
    }

    public SuppressFHSSModel getRecord() { return record; }

    public Integer getId() { return id; }

}
