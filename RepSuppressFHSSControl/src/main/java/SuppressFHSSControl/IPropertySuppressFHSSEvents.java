package SuppressFHSSControl;

public interface IPropertySuppressFHSSEvents {
    public void OnIsAddRecord(Boolean b);
    public void OnIsChangeRecord(Boolean b);
}
