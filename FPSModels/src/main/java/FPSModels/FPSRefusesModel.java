package FPSModels;


public class FPSRefusesModel {

    private Led refusalTemperature;
    private Led refusalKSV;
    private Led refusalAmplifier;
    private Led refusalFilters;
    private Led refusalPowerSupply;
    private Led refusalFeeder;
    private byte refusalPercentPower;
    private Led refusalRefuse;
    private Led refusalControl;

    public FPSRefusesModel() {
        this.refusalTemperature = Led.Empty;
        this.refusalKSV = Led.Empty;
        this.refusalControl = Led.Empty;
        this.refusalRefuse = Led.Empty;
        this.refusalPowerSupply = Led.Empty;
        this.refusalFilters = Led.Empty;
        this.refusalFeeder = Led.Empty;
        this.refusalAmplifier = Led.Empty;
    }



    public FPSRefusesModel(Led refusalTemperature, Led refusalAmplifier, Led refusalFeeder, Led refusalFilters, Led refusalKSV
            , byte refusalPercentPower, Led refusalPowerSupply, Led refusalRefuse, Led refusalControl) {

        this.refusalAmplifier = refusalAmplifier;
        this.refusalFeeder = refusalFeeder;
        this.refusalFilters = refusalFilters;
        this.refusalKSV = refusalKSV;
        this.refusalPercentPower = refusalPercentPower;
        this.refusalPowerSupply = refusalPowerSupply;
        this.refusalRefuse = refusalRefuse;
        this.refusalTemperature = refusalTemperature;
        this.refusalControl = refusalControl;

    }

    public Led getRefusalTemperature() {
        return refusalTemperature;
    }

    public void setRefusalTemperature(Led refusalTemperature) {
        this.refusalTemperature = refusalTemperature;
    }

    public Led getRefusalKSV() {
        return refusalKSV;
    }

    public void setRefusalKSV(Led refusalKSV) {
        this.refusalKSV = refusalKSV;
    }

    public Led getRefusalAmplifier() {
        return refusalAmplifier;
    }

    public void setRefusalAmplifier(Led refusalAmplifier) {
        this.refusalAmplifier = refusalAmplifier;
    }

    public Led getRefusalFilters() { return refusalFilters; }

    public void setRefusalFilters(Led refusalFilters) {
        this.refusalFilters = refusalFilters;
    }

    public Led getRefusalPowerSupply() {
        return refusalPowerSupply;
    }

    public void setRefusalPowerSupply(Led refusalPowerSupply) {
        this.refusalPowerSupply = refusalPowerSupply;
    }

    public Led getRefusalFeeder() {
        return refusalFeeder;
    }

    public void setRefusalFeeder(Led refusalFeeder) {
        this.refusalFeeder = refusalFeeder;
    }

    public Led getRefusalRefuse() {
        return refusalRefuse;
    }

    public void setRefusalRefuse(Led refusalRefuse) {
        this.refusalRefuse = refusalRefuse;
    }

    public Byte getRefusalPercentPower() {
        return refusalPercentPower;
    }

    public void setRefusalPercentPower(Byte refusalPercentPower) {
        this.refusalPercentPower = refusalPercentPower;
    }

    public void setRefusalControl(Led refusalControl) {
        this.refusalControl = refusalControl;
    }

    public Led getRefusalControl() {
        return refusalControl;
    }



}
