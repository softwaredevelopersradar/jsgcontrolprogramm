package FPSModels;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


public class SuppressFHSSModel {

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty();
    private SimpleIntegerProperty freqMin = new SimpleIntegerProperty();
    private SimpleIntegerProperty freqMax = new SimpleIntegerProperty();
    private InterferenceParams interferenceParams = new InterferenceParams();

    public SuppressFHSSModel(){

    }

    public SuppressFHSSModel(int id, boolean isCheck, int freqMin, int freqMax, InterferenceParams interferenceParams) {
        this.id = new SimpleIntegerProperty(id);
        this.isCheck = new SimpleBooleanProperty(isCheck);
        this.freqMin = new SimpleIntegerProperty(freqMin);
        this.freqMax = new SimpleIntegerProperty(freqMax);
        this.interferenceParams = new InterferenceParams(interferenceParams);
    }

    public int getId() { return id.get(); }
    public void setId(int value) { id.set(value); }

    public SimpleBooleanProperty isCheckProperty() { return isCheck; }
    public Boolean getIsCheck(){ return isCheck.get();}
    public void setIsCheck(boolean value){ isCheck.set(value);}

    public int getFreqMin() { return freqMin.get(); }
    public SimpleIntegerProperty freqMinProperty() { return freqMin; }
    public void setFreqMin(int freqMin) { this.freqMin.set(freqMin); }

    public int getFreqMax() { return freqMax.get(); }
    public SimpleIntegerProperty freqMaxProperty() { return freqMax; }
    public void setFreqMax(int freqMax) { this.freqMax.set(freqMax); }

    public InterferenceParams getInterferenceParams() { return interferenceParams; }
    public void setInterferenceParams(InterferenceParams interferenceParams) { this.interferenceParams = interferenceParams; }

}
