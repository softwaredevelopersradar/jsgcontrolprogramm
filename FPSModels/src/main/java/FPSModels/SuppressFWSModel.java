package FPSModels;


import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;


public class SuppressFWSModel {

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty();
    private SimpleIntegerProperty frequency = new SimpleIntegerProperty();
    private InterferenceParams interferenceParams = new InterferenceParams();

    public SuppressFWSModel() {
        
    }

    public SuppressFWSModel(int id, boolean isCheck, int frequency, InterferenceParams interferenceParams) {

        this.id = new SimpleIntegerProperty(id);
        this.isCheck = new SimpleBooleanProperty(isCheck);
        this.frequency = new SimpleIntegerProperty(frequency);
        this.interferenceParams = new InterferenceParams(interferenceParams);
      
    }

    public int getId() {
        return this.id.get();
    }
    public void setId(int value) {
        this.id.set(value);
    } 
    public SimpleBooleanProperty isCheckProperty() {
        return this.isCheck;
    }

    public Boolean getIsCheck() {
        return this.isCheck.get();
    }

    public void setIsCheck(boolean value) {
        this.isCheck.set(value);
    }

    public int getFrequency() {
        return this.frequency.get();
    }

    public SimpleIntegerProperty frequencyProperty() {
        return this.frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency.set(frequency);
    }

    public InterferenceParams getInterferenceParams() {
        return this.interferenceParams;
    }

    public void setInterferenceParams(InterferenceParams interferenceParams) {
        this.interferenceParams = interferenceParams;
    }


}
