package FPSControlDebug;

import FPSControlDebug.TransferClient.ClientJSG;
import FPSControlDebug.TransferClient.GrpcClientModel;
import FPSModels.*;
import FPSTableDiscriptionsControl.TableDescriptions;
import FPSTableParams.TableParams;
import FPSValuesLib.ConnectionsStates;
import FPSValuesLib.Suppress.InitInterferenceParams;
import PackageJSG.JSG;
import SuppressFHSSControl.*;
import SuppressFWSControl.*;
import io.grpc.netty.shaded.io.netty.util.internal.StringUtil;
import io.grpc.stub.AbstractAsyncStub;
import io.grpc.stub.StreamObserver;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import jdk.jfr.Frequency;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class FPSControlDebug extends AnchorPane implements ITableSuppressFWSEvents, ITableSuppressFHSSEvents, Initializable {
    private GrpcClientModel clientJSG;
    private GrpcClientModel clientServer;
    // region UserControls
    @FXML
    public SuppressFWSControl ucSuppressFWS;
    @FXML
    public SuppressFHSSControl ucSuppressFHSS;
    @FXML
    public TableDescriptions ucDescriptionsTable;
    @FXML
    public TableParams ucParamsTable;
    // endregion

    @FXML
    public ComboBox<String> power_percentage;
    @FXML
    public ComboBox<Integer> fft;

    @FXML
    public Button bOnOffPower;
    @FXML
    public Button bOnOffRadiation;
    @FXML
    public Button bStatusRequestFPS;
    @FXML
    public Label Version;

    @FXML
    public Button bConnect;
    @FXML
    public Button bConnectJSG;
    @FXML
    public TextArea LogFPS;
    @FXML
    public Spinner<Integer> duration;

    @FXML
    public Tooltip tooltip;

    public ConnectionsStates ServerConnectionsState;
    private Boolean RadiationStatus = false;
    private Boolean Set2Filter = false;
    private Boolean PowerSupply = false;
    public SuppressFWSModel suppressFWSModel = new SuppressFWSModel();
    FPSModel model = new FPSModel();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss:S");

    public FPSControlDebug() {
    }

    //Кнопка очистки
    public void ClickButtonClear(ActionEvent actionEvent) {
        ObservableList<ErrorsModel> tempList = FXCollections.observableArrayList(new ErrorsModel[]{});
        ucDescriptionsTable.tableDescription.setItems(tempList);
        ObservableList<FPSModel> templist1 = FXCollections.observableArrayList(new FPSModel[]{});
        ucParamsTable.tableStatus.setItems(templist1);
        LogFPS.clear();
    }

    // Кнопка включения питания УМ
    public void ClickButtonOnOffPower(ActionEvent actionEvent) {

        if (PowerSupply) {
            clientJSG.asyncStub.setPowerSupply(JSG.PowerSupplyMessage.newBuilder().setPowerSupply(false).build(), new StreamObserver<JSG.DefaulAmResponseMessage>() {
                @Override
                public void onNext(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {

                    switch (defaulAmResponseMessage.getResult()) {
                        case Executed -> {
                            PowerSupply = false;
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: Power OFF " + "\n");

                                        Platform.runLater(new Runnable() {
                                            @Override
                                            public void run() {
                                                bOnOffPower.setText("Вкл. питание");

                                            }
                                        });




//                            clientJSG.asyncStub.statusRequestAmp(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.StateAmResponseMessage>() {
//                                @Override
//                                public void onNext(JSG.StateAmResponseMessage stateAmResponseMessage) {
//                                    var errorCode = stateAmResponseMessage.getJsgError();
//                                    errorCode.getCommandCode();
//                                    var errors = errorCode.getErrors();
//                                    StringBuilder temp = new StringBuilder();
//                                    for (int i = 0; i < errors.size(); i++) {
//                                        temp.append(errors.toByteArray()[i]);
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                if (!temp.append(errors.toByteArray()).isEmpty()) {
//                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: " + temp.toString() + "\n");
//                                                }
//                                            }
//                                        });
//                                    }
//
//
//                                    if (stateAmResponseMessage.getHasFailure()) {
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Amplifier \n");
//                                            }
//                                        });
//                                    }
//
//                                    model.setPower(stateAmResponseMessage.getPower());
//                                    model.setKsv(stateAmResponseMessage.getVSWR());
//                                    model.setSupply(stateAmResponseMessage.getHasPower());
//                                    model.setFilter(stateAmResponseMessage.getSetted2Filter());
//                                    model.setRadiation(stateAmResponseMessage.getHasRadiation());
//                                    model.setTemperature(stateAmResponseMessage.getTemperature());
//                                    model.setControl(stateAmResponseMessage.getIsLocalControl());
//                                    ucParamsTable.listParamFPS.clear();
//                                    ucParamsTable.listParamFPS.add(model);
//                                    ucParamsTable.UpdateStatus(ucParamsTable.listParamFPS);
//
//                                    if (stateAmResponseMessage.getHasPower()) {
//                                        PowerSupply = true;
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                bOnOffPower.setText("Откл. питание");
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: power ON \n");
//                                            }
//                                        });
//                                    } else {
//                                        PowerSupply = false;
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                bOnOffPower.setText("Вкл. питание");
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: power OFF \n");
//                                            }
//                                        });
//                                    }
//
//                                    if (stateAmResponseMessage.getIsReady()) {
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: amplifier is ready \n");
//                                            }
//                                        });
//                                    }
//
//                                }
//
//
//                                @Override
//                                public void onError(Throwable throwable) {
//                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error \n");
//                                }
//
//                                @Override
//                                public void onCompleted() {
//
//                                }
//                            });

                        }
                        case Alarm -> {

                            var info = defaulAmResponseMessage.getAlarmInfo();
                            LogFPS.appendText(String.format(simpleDateFormat.format(new Date()) + "received: amplifier alarm: %b, filters alarm: %b, power alarm: %b",
                                    info.getAmplifierAlarm(), info.getFiltersAlarm(), info.getPowerAlarm()) + "\n");

                        }
                        case RemoteControlError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: remote Control Error \n");

                        }
                        case TypingError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: typing Error \n");
                        }
                        case LocalControl -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: local control \n");
                        }
                        case UNRECOGNIZED -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: unknown request \n");
                        }
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error" + "\n");
                }

                @Override
                public void onCompleted() {

                }
            });
        } else {
            clientJSG.asyncStub.setPowerSupply(JSG.PowerSupplyMessage.newBuilder().setPowerSupply(true).build(), new StreamObserver<JSG.DefaulAmResponseMessage>() {
                @Override
                public void onNext(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {

                    switch (defaulAmResponseMessage.getResult()) {
                        case Executed -> {
                            PowerSupply = true;

                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: Power ON " + "\n");

                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    bOnOffPower.setText("Откл. питание");
                                }
                            });

//                            clientJSG.asyncStub.statusRequestAmp(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.StateAmResponseMessage>() {
//                                @Override
//                                public void onNext(JSG.StateAmResponseMessage stateAmResponseMessage) {
//                                    var errorCode = stateAmResponseMessage.getJsgError();
//                                    errorCode.getCommandCode();
//                                    var errors = errorCode.getErrors();
//                                    StringBuilder temp = new StringBuilder();
//                                    for (int i = 0; i < errors.size(); i++) {
//                                        temp.append(errors.toByteArray()[i]);
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                if (!temp.append(errors.toByteArray()).isEmpty()) {
//                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: " + temp.toString() + "\n");
//                                                }
//                                            }
//                                        });
//                                    }
//
//
//                                    if (stateAmResponseMessage.getHasFailure()) {
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Amplifier \n");
//                                            }
//                                        });
//                                    }
//
//                                    model.setPower(stateAmResponseMessage.getPower());
//                                    model.setKsv(stateAmResponseMessage.getVSWR());
//                                    model.setSupply(stateAmResponseMessage.getHasPower());
//                                    model.setFilter(stateAmResponseMessage.getSetted2Filter());
//                                    model.setRadiation(stateAmResponseMessage.getHasRadiation());
//                                    model.setTemperature(stateAmResponseMessage.getTemperature());
//                                    model.setControl(stateAmResponseMessage.getIsLocalControl());
//                                    ucParamsTable.listParamFPS.clear();
//                                    ucParamsTable.listParamFPS.add(model);
//                                    ucParamsTable.UpdateStatus(ucParamsTable.listParamFPS);
//
//                                    if (stateAmResponseMessage.getHasPower()) {
//                                        PowerSupply = true;
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                bOnOffPower.setText("Откл. питание");
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: power ON \n");
//                                            }
//                                        });
//                                    } else {
//                                        PowerSupply = false;
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                bOnOffPower.setText("Вкл. питание");
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: power OFF \n");
//                                            }
//                                        });
//                                    }
//
//                                    if (stateAmResponseMessage.getIsReady()) {
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: amplifier is ready \n");
//                                            }
//                                        });
//                                    }
//
//                                }
//
//
//                                @Override
//                                public void onError(Throwable throwable) {
//                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: Eerror \n");
//                                }
//
//                                @Override
//                                public void onCompleted() {
//
//                                }
//                            });

                        }
                        case Alarm -> {

                            var info = defaulAmResponseMessage.getAlarmInfo();
                            LogFPS.appendText(String.format(simpleDateFormat.format(new Date()) + " received: amplifier alarm: %b, filters alarm: %b, power alarm: %b",
                                    info.getAmplifierAlarm(), info.getFiltersAlarm(), info.getPowerAlarm()) + "\n");

                        }
                        case RemoteControlError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: remote Control Error \n");

                        }
                        case TypingError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: typing Error \n");
                        }
                        case LocalControl -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: local control \n");
                        }
                        case UNRECOGNIZED -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: unknown request \n");
                        }
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error" + "\n");
                }

                @Override
                public void onCompleted() {

                }
            });
        }
    }

    // Кнопка запроса телесигнализации
    public void ClickButtonTS(ActionEvent actionEvent) {
        clientJSG.asyncStub.telesignalizationRequest(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.TelesignalResponseMessage>() {
            @Override
            public void onNext(JSG.TelesignalResponseMessage telesignalResponseMessage) {

                var failureTS = telesignalResponseMessage.getFailure();
                var isReady = telesignalResponseMessage.getIsReady();
                var isWorking = telesignalResponseMessage.getIsWorking();
                if (failureTS) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: errors TS  \n");
                } else if (isReady) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: TS is ready  \n");
                } else if (isWorking) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: TS is working  \n");
                } else {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: no answer TS  \n");
                }
            }

            @Override
            public void onError(Throwable throwable) {
                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error  \n");
            }

            @Override
            public void onCompleted() {

            }
        });

    }

    // Кнопка выхода из режима ППРЧ
    public void ClickFHSSOff(ActionEvent actionEvent) {
        clientJSG.asyncStub.setFHSSOff(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.DefaultResponse>() {
            @Override
            public void onNext(JSG.DefaultResponse defaultResponse) {
                if (defaultResponse.getIsSuccessed()) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: command FHSS off is successed \n");
                } else {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: Error set command FHSS off \n");
                }

            }

            @Override
            public void onError(Throwable throwable) {
                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error  \n");
            }

            @Override
            public void onCompleted() {

            }
        });
    }

    // Кнопка выхода из режима ППРЧ
    public void ClickRadiationOff(ActionEvent actionEvent) {
        clientJSG.asyncStub.radiationOff(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.DefaultResponse>() {
            @Override
            public void onNext(JSG.DefaultResponse defaultResponse) {
                if (defaultResponse.getIsSuccessed()) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: command Radiation Off is successed \n");
                } else {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error set Radiation Off command \n");
                }

            }

            @Override
            public void onError(Throwable throwable) {
                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error set Radiation Off command \n");
            }

            @Override
            public void onCompleted() {

            }
        });
    }

    // Кнопка включения/отключения 2 диапазона ФГ
    public void ClickButtonFilter(ActionEvent actionEvent) {
        FPSModel model = new FPSModel();
        if (Set2Filter) {
            clientJSG.asyncStub.setFilterAm(JSG.FilterMessage.newBuilder().setFilter(JSG.Filter.LOW).build(), new StreamObserver<JSG.DefaulAmResponseMessage>() {
                @Override
                public void onNext(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {

                    switch (defaulAmResponseMessage.getResult()) {
                        case Executed -> {

                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " sent: command Set 1 filter to JSG" + "\n");
                            try {
                                Thread.sleep(300);
                                clientJSG.asyncStub.statusRequestAmp(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.StateAmResponseMessage>() {
                                    @Override
                                    public void onNext(JSG.StateAmResponseMessage stateAmResponseMessage) {
                                        var errorCode = stateAmResponseMessage.getJsgError();
                                        errorCode.getCommandCode();
                                        var errors = errorCode.getErrors();
                                        StringBuilder temp = new StringBuilder();
                                        for (int i = 0; i < errors.size(); i++) {
                                            temp.append(errors.toByteArray()[i]);
                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (!temp.append(errors.toByteArray()).isEmpty()) {
                                                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: " + temp.toString() + "\n");
                                                    }
                                                }
                                            });
                                        }


                                        if (stateAmResponseMessage.getHasFailure()) {
                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Amplifier \n");
                                                }
                                            });
                                        }

                                        model.setPower(stateAmResponseMessage.getPower());
                                        model.setKsv(stateAmResponseMessage.getVSWR());
                                        model.setSupply(stateAmResponseMessage.getHasPower());
                                        model.setFilter(stateAmResponseMessage.getSetted2Filter());
                                        model.setRadiation(stateAmResponseMessage.getHasRadiation());
                                        model.setTemperature(stateAmResponseMessage.getTemperature());
                                        model.setControl(stateAmResponseMessage.getIsLocalControl());
                                        ucParamsTable.listParamFPS.clear();
                                        ucParamsTable.listParamFPS.add(model);
                                        ucParamsTable.UpdateStatus(ucParamsTable.listParamFPS);

                                        if (stateAmResponseMessage.getSetted2Filter()) {
                                            Set2Filter = true;
                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: set 2 filter \n");
                                                }
                                            });
                                        } else {
                                            Set2Filter = false;
                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: set 1 filter \n");
                                                }
                                            });
                                        }

                                        if (stateAmResponseMessage.getIsReady()) {
                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: amplifier is ready \n");
                                                }
                                            });
                                        }

                                    }


                                    @Override
                                    public void onError(Throwable throwable) {
                                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error \n");
                                    }

                                    @Override
                                    public void onCompleted() {

                                    }
                                });

                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                        case Alarm -> {
                            Set2Filter = false;
                            var info = defaulAmResponseMessage.getAlarmInfo();
                            LogFPS.appendText(String.format(simpleDateFormat.format(new Date()) + "received: amplifier alarm: %b, filters alarm: %b, power alarm: %b",
                                    info.getAmplifierAlarm(), info.getFiltersAlarm(), info.getPowerAlarm()) + "\n");

                        }
                        case RemoteControlError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: remote Control Error \n");

                        }
                        case TypingError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: typing Error \n");
                        }
                        case LocalControl -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: local control \n");
                        }
                        case UNRECOGNIZED -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: unknown request \n");
                        }
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error" + "\n");
                }

                @Override
                public void onCompleted() {

                }
            });
        } else {
            clientJSG.asyncStub.setFilterAm(JSG.FilterMessage.newBuilder().setFilter(JSG.Filter.HIGH).build(), new StreamObserver<JSG.DefaulAmResponseMessage>() {
                @Override
                public void onNext(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {

                    switch (defaulAmResponseMessage.getResult()) {
                        case Executed -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " sent: command Set 2 filter to JSG" + "\n");
                            try {
                                Thread.sleep(300);
                                clientJSG.asyncStub.statusRequestAmp(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.StateAmResponseMessage>() {
                                    @Override
                                    public void onNext(JSG.StateAmResponseMessage stateAmResponseMessage) {
                                        var errorCode = stateAmResponseMessage.getJsgError();
                                        errorCode.getCommandCode();
                                        var errors = errorCode.getErrors();
                                        StringBuilder temp = new StringBuilder();
                                        for (int i = 0; i < errors.size(); i++) {
                                            temp.append(errors.toByteArray()[i]);
                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (!temp.append(errors.toByteArray()).isEmpty()) {
                                                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: " + temp.toString() + "\n");
                                                    }
                                                }
                                            });
                                        }


                                        if (stateAmResponseMessage.getHasFailure()) {
                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Amplifier \n");
                                                }
                                            });
                                        }

                                        model.setPower(stateAmResponseMessage.getPower());
                                        model.setKsv(stateAmResponseMessage.getVSWR());
                                        model.setSupply(stateAmResponseMessage.getHasPower());
                                        model.setFilter(stateAmResponseMessage.getSetted2Filter());
                                        model.setRadiation(stateAmResponseMessage.getHasRadiation());
                                        model.setTemperature(stateAmResponseMessage.getTemperature());
                                        model.setControl(stateAmResponseMessage.getIsLocalControl());
                                        ucParamsTable.listParamFPS.clear();
                                        ucParamsTable.listParamFPS.add(model);
                                        ucParamsTable.UpdateStatus(ucParamsTable.listParamFPS);

                                        if (stateAmResponseMessage.getSetted2Filter()) {
                                            Set2Filter = true;
                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: set 2 filter \n");
                                                }
                                            });
                                        } else {
                                            Set2Filter = false;
                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: set 1 filter \n");
                                                }
                                            });
                                        }

                                        if (stateAmResponseMessage.getIsReady()) {
                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: amplifier is ready \n");
                                                }
                                            });
                                        }

                                    }


                                    @Override
                                    public void onError(Throwable throwable) {
                                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error \n");
                                    }

                                    @Override
                                    public void onCompleted() {

                                    }
                                });


                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                        case Alarm -> {
                            Set2Filter = false;
                            var info = defaulAmResponseMessage.getAlarmInfo();
                            LogFPS.appendText(String.format(simpleDateFormat.format(new Date()) + " received: amplifier alarm: %b, filters alarm: %b, power alarm: %b",
                                    info.getAmplifierAlarm(), info.getFiltersAlarm(), info.getPowerAlarm()) + "\n");

                        }
                        case RemoteControlError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: remote Control Error \n");

                        }
                        case TypingError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: typing Error \n");
                        }
                        case LocalControl -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: local control \n");
                        }
                        case UNRECOGNIZED -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: unknown request \n");
                        }
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error" + "\n");
                }

                @Override
                public void onCompleted() {

                }
            });
        }
    }

    //Кнопка отправки параметров ФРЧ и вкл. излучения
    public void ClickSendFWS(ActionEvent actionEvent) {
        var list = ucSuppressFWS.tableSuppressFWS.getItems().stream().toList().stream()
                .filter(x -> x.getIsCheck() == true)
                .collect(Collectors.toList());

//    var list = ucSuppressFWS.tableSuppressFWS.getItems().stream().toList();
    var jammingParams = new ArrayList<JSG.JammingParamsMessage>();
    var Duration = duration.getValue();

    for (var element : list) {
        JSG.InterferenceParamsMessage params = JSG.InterferenceParamsMessage.newBuilder().setDeviation(element.getInterferenceParams().deviation).setDuration(element.getInterferenceParams().duration).setManipulation(element.getInterferenceParams().manipulation).setModulation(element.getInterferenceParams().modulation).build();
        JSG.JammingParamsMessage jammingParam = JSG.JammingParamsMessage.newBuilder().setFrequencyHz(element.getFrequency()).setInterferenceParams(params).build();
        jammingParams.add(jammingParam);
    }

    clientJSG.asyncStub.setParamFFJam(JSG.FFJammingMessage.newBuilder().setDuration(Duration).addAllParams(jammingParams).build(), new StreamObserver<JSG.DefaultResponse>() {
        @Override
        public void onNext(JSG.DefaultResponse defaultResponse) {
            if (defaultResponse.getIsSuccessed()) {
                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: params FWS set \n");
            } else {
                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: params FWS not set \n");
            }
        }

        @Override
        public void onError(Throwable throwable) {
            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: params FWS not set \n");
        }

        @Override
        public void onCompleted() {

        }
    });
}


    //Кнопка отправки параметров ППРЧ
    public void ClickSendFHSS(ActionEvent actionEvent) {
        var list = ucSuppressFHSS.tableSuppressFHSS.getItems().stream().toList();
        var jammingParams = new ArrayList<JSG.JammingParamsMessage>();
        var currentFFT = fft.getValue();
        byte FFT = 0;
        switch (currentFFT) {
            case 12208->
                FFT = 1;

            case 6104->
                FFT = 2;

            case 3052->
                FFT = 3;
        }

        for (var element : list) {
            JSG.InterferenceParamsMessage params = JSG.InterferenceParamsMessage.newBuilder().setDeviation(element.getInterferenceParams().deviation).setDuration(element.getInterferenceParams().duration).setManipulation(element.getInterferenceParams().manipulation).setModulation(element.getInterferenceParams().modulation).build();
            JSG.JammingParamsMessage jammingParam = JSG.JammingParamsMessage.newBuilder().setFrequencyHz(element.getFreqMin()).setInterferenceParams(params).build();
            jammingParams.add(jammingParam);
        }
        clientJSG.asyncStub.setParamFHSSAm(JSG.FHSSJammingMessage.newBuilder().setDuration(1).addAllParams(jammingParams).setCodeFFT(FFT).build(), new StreamObserver<JSG.DefaultResponse>() {
            @Override
            public void onNext(JSG.DefaultResponse defaultResponse) {
                if (defaultResponse.getIsSuccessed()) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: params FHSS set \n");
                } else {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: params FHSS not set \n");
                }
            }

            @Override
            public void onError(Throwable throwable) {
                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: params FHSS not set \n");
            }

            @Override
            public void onCompleted() {

            }
        });
    }

    //Кнопка включения/отключения излучения УМ
    public void ClickButtonOnOffRadiation(ActionEvent actionEvent) {
        if (RadiationStatus) {
            clientJSG.asyncStub.setRadiation(JSG.RadiationMessage.newBuilder().setRadiation(false).build(), new StreamObserver<JSG.DefaulAmResponseMessage>() {
                @Override
                public void onNext(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {

                    switch (defaulAmResponseMessage.getResult()) {
                        case Executed -> {
                            RadiationStatus = false;
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: Radiation OFF " + "\n");
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    bOnOffRadiation.setText("Вкл. изл.");

                                }
                            });

//                            clientJSG.asyncStub.statusRequestAmp(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.StateAmResponseMessage>() {
//                                @Override
//                                public void onNext(JSG.StateAmResponseMessage stateAmResponseMessage) {
//                                    var errorCode = stateAmResponseMessage.getJsgError();
//                                    errorCode.getCommandCode();
//                                    var errors = errorCode.getErrors();
//                                    StringBuilder temp = new StringBuilder();
//                                    for (int i = 0; i < errors.size(); i++) {
//                                        temp.append(errors.toByteArray()[i]);
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                if (!temp.append(errors.toByteArray()).isEmpty()) {
//                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: " + temp.toString() + "\n");
//                                                }
//                                            }
//                                        });
//                                    }
//
//
//                                    if (stateAmResponseMessage.getHasFailure()) {
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Amplifier \n");
//                                            }
//                                        });
//                                    }
//
//                                    model.setPower(stateAmResponseMessage.getPower());
//                                    model.setKsv(stateAmResponseMessage.getVSWR());
//                                    model.setSupply(stateAmResponseMessage.getHasPower());
//                                    model.setFilter(stateAmResponseMessage.getSetted2Filter());
//                                    model.setRadiation(stateAmResponseMessage.getHasRadiation());
//                                    model.setTemperature(stateAmResponseMessage.getTemperature());
//                                    model.setControl(stateAmResponseMessage.getIsLocalControl());
//                                    ucParamsTable.listParamFPS.clear();
//                                    ucParamsTable.listParamFPS.add(model);
//                                    ucParamsTable.UpdateStatus(ucParamsTable.listParamFPS);
//
//                                    if (stateAmResponseMessage.getHasRadiation()) {
//                                        RadiationStatus = true;
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                bOnOffRadiation.setText("Откл. изл.");
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: radiation ON \n");
//                                            }
//                                        });
//                                    } else {
//                                        RadiationStatus = false;
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                bOnOffRadiation.setText("Вкл. изл.");
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: radiation OFF \n");
//                                            }
//                                        });
//                                    }
//
//                                    if (stateAmResponseMessage.getIsReady()) {
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: amplifier is ready \n");
//                                            }
//                                        });
//                                    }
//
//                                }
//
//
//                                @Override
//                                public void onError(Throwable throwable) {
//                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error \n");
//                                }
//
//                                @Override
//                                public void onCompleted() {
//
//                                }
//                            });
                        }
                        case Alarm -> {
                            var info = defaulAmResponseMessage.getAlarmInfo();
                            LogFPS.appendText(String.format(simpleDateFormat.format(new Date()) + " received: amplifier alarm: %b, filters alarm: %b, power alarm: %b",
                                    info.getAmplifierAlarm(), info.getFiltersAlarm(), info.getPowerAlarm()) + "\n");

                        }
                        case RemoteControlError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: remote Control Error \n");

                        }
                        case TypingError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: typing Error \n");
                        }
                        case LocalControl -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: local control \n");
                        }
                        case UNRECOGNIZED -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: unknown request \n");
                        }
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error \n");
                }

                @Override
                public void onCompleted() {

                }
            });
        } else {
            clientJSG.asyncStub.setRadiation(JSG.RadiationMessage.newBuilder().setRadiation(true).build(), new StreamObserver<JSG.DefaulAmResponseMessage>() {
                @Override
                public void onNext(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {
                    switch (defaulAmResponseMessage.getResult()) {
                        case Executed -> {

                            RadiationStatus = true;
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: Radiation ON " + "\n");
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    bOnOffRadiation.setText("Откл. изл.");
                                }
                            });

//                            clientJSG.asyncStub.statusRequestAmp(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.StateAmResponseMessage>() {
//                                @Override
//                                public void onNext(JSG.StateAmResponseMessage stateAmResponseMessage) {
//                                    var errorCode = stateAmResponseMessage.getJsgError();
//                                    errorCode.getCommandCode();
//                                    var errors = errorCode.getErrors();
//                                    StringBuilder temp = new StringBuilder();
//                                    for (int i = 0; i < errors.size(); i++) {
//                                        temp.append(errors.toByteArray()[i]);
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                if (!temp.append(errors.toByteArray()).isEmpty()) {
//                                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: " + temp.toString() + "\n");
//                                                }
//                                            }
//                                        });
//                                    }
//
//
//                                    if (stateAmResponseMessage.getHasFailure()) {
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Amplifier \n");
//                                            }
//                                        });
//                                    }
//
//                                    model.setPower(stateAmResponseMessage.getPower());
//                                    model.setKsv(stateAmResponseMessage.getVSWR());
//                                    model.setSupply(stateAmResponseMessage.getHasPower());
//                                    model.setFilter(stateAmResponseMessage.getSetted2Filter());
//                                    model.setRadiation(stateAmResponseMessage.getHasRadiation());
//                                    model.setTemperature(stateAmResponseMessage.getTemperature());
//                                    model.setControl(stateAmResponseMessage.getIsLocalControl());
//                                    ucParamsTable.listParamFPS.clear();
//                                    ucParamsTable.listParamFPS.add(model);
//                                    ucParamsTable.UpdateStatus(ucParamsTable.listParamFPS);
//
//                                    if (stateAmResponseMessage.getHasRadiation()) {
//                                        RadiationStatus = true;
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                bOnOffRadiation.setText("Откл. изл.");
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: radiation ON \n");
//                                            }
//                                        });
//                                    } else {
//                                        RadiationStatus = false;
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                bOnOffRadiation.setText("Вкл. изл.");
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: radiation OFF \n");
//                                            }
//                                        });
//                                    }
//
//                                    if (stateAmResponseMessage.getIsReady()) {
//                                        Platform.runLater(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: amplifier is ready \n");
//                                            }
//                                        });
//                                    }
//
//                                }
//
//
//                                @Override
//                                public void onError(Throwable throwable) {
//                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error \n");
//                                }
//
//                                @Override
//                                public void onCompleted() {
//
//                                }
//                            });
                        }
                        case Alarm -> {
                            var info = defaulAmResponseMessage.getAlarmInfo();
                            LogFPS.appendText(String.format(simpleDateFormat.format(new Date()) + " received: amplifier alarm: %b, filters alarm: %b, power alarm: %b",
                                    info.getAmplifierAlarm(), info.getFiltersAlarm(), info.getPowerAlarm()) + "\n");
                        }
                        case RemoteControlError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: remote Control Error \n");
                        }
                        case TypingError -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: typing Error \n");
                        }
                        case LocalControl -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: local control \n");
                        }
                        case UNRECOGNIZED -> {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: unknown request \n");
                        }
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error \n");
                }

                @Override
                public void onCompleted() {

                }
            });
        }

    }

    //запрос состояния ФПС (получаем версию прошивки, код ошибки, режим)
    public void ClickStatusRequestFPS(ActionEvent actionEvent) {
        LogFPS.appendText(simpleDateFormat.format(new Date()) + " sent: JSG status request" + "\n");
        clientJSG.asyncStub.statusRequest(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.StateJsgResponseMessage>() {
            @Override
            public void onNext(JSG.StateJsgResponseMessage stateJsgResponseMessage) {

                var version = stateJsgResponseMessage
                        .getFirmwareVersionList()
                        .stream().map(String::valueOf).collect(Collectors.joining(","));
                var errorCode = stateJsgResponseMessage.getJsgError();
                errorCode.getCommandCode();
                var errors = errorCode.getErrors();
                StringBuilder temp = new StringBuilder();
                for (int i = 0; i < errors.size(); i++) {
                    temp.append(errors.toByteArray()[i]);
                }
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        Version.setText(version);
                        if (!temp.isEmpty()) {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: " + temp + "\n");
                        }
                    }
                });

                switch (stateJsgResponseMessage.getMode()) {

                    case STOP_RI -> {
                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: mode - STOP/RI \n");
                    }
                    case STOP -> {
                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: mode - STOP \n");
                    }
                    case FF_JAMMING -> {
                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: mode - FF Jamming \n");
                    }
                    case FHSS_JAMMING -> {
                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: mode - FHSS Jamming \n");
                    }
                    case DURATION_MEASUREMENT_FHSS -> {
                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: mode - Duration measurement FHSS \n");
                    }
                    case VOICE -> {
                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: mode - Voice jamming \n");
                    }
                    case UNRECOGNIZED -> {
                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: unknown request \n");
                    }
                }
            }

            @Override
            public void onError(Throwable throwable) {
                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error \n");
            }

            @Override
            public void onCompleted() {

            }
        });
    }

    //установка %мощности УМ
    public void ClickButtonOk(ActionEvent actionEvent) {
        var currentPower = power_percentage.getValue();
        switch (currentPower) {
            case "100" -> {
                clientJSG.asyncStub.setPowerLevel(JSG.PowerLevelMessage.newBuilder().setPowerCode(JSG.PowerCode.P_100).build(), new StreamObserver<JSG.DefaulAmResponseMessage>() {
                    @Override
                    public void onNext(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {
                        switch (defaulAmResponseMessage.getResult()) {
                            case Executed -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: set level 100% \n");
                            }
                            case Alarm -> {
                                if (defaulAmResponseMessage.getAlarmInfo().getAmplifierAlarm()) {
                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Amplifier \n");
                                } else if (defaulAmResponseMessage.getAlarmInfo().getFiltersAlarm()) {
                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Filters \n");
                                } else {
                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Power Supply \n");
                                }

                            }
                            case RemoteControlError -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: remote Control Error \n");
                            }
                            case TypingError -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: typing Error \n");
                            }
                            case LocalControl -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: local Control \n");
                            }
                            case UNRECOGNIZED -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: unknown request \n");
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error \n");
                    }

                    @Override
                    public void onCompleted() {

                    }
                });
            }
            case "50" -> {
                clientJSG.asyncStub.setPowerLevel(JSG.PowerLevelMessage.newBuilder().setPowerCode(JSG.PowerCode.P_50).build(), new StreamObserver<JSG.DefaulAmResponseMessage>() {
                    @Override
                    public void onNext(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {
                        switch (defaulAmResponseMessage.getResult()) {
                            case Executed -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: set level 50% \n");
                            }
                            case Alarm -> {
                                if (defaulAmResponseMessage.getAlarmInfo().getAmplifierAlarm()) {
                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Amplifier \n");
                                } else if (defaulAmResponseMessage.getAlarmInfo().getFiltersAlarm()) {
                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Filters \n");
                                } else {
                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Power Supply \n");
                                }
                            }
                            case RemoteControlError -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: remote Control Error \n");
                            }
                            case TypingError -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: typing Error \n");
                            }
                            case LocalControl -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: local Control \n");
                            }
                            case UNRECOGNIZED -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: unknown request \n");
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        LogFPS.appendText(" received: error Set level 50% \n");
                    }

                    @Override
                    public void onCompleted() {

                    }
                });
            }
            case "25" -> {
                clientJSG.asyncStub.setPowerLevel(JSG.PowerLevelMessage.newBuilder().setPowerCode(JSG.PowerCode.P_25).build(), new StreamObserver<JSG.DefaulAmResponseMessage>() {
                    @Override
                    public void onNext(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {
                        switch (defaulAmResponseMessage.getResult()) {
                            case Executed -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: set level 25% \n");
                            }
                            case Alarm -> {
                                if (defaulAmResponseMessage.getAlarmInfo().getAmplifierAlarm()) {
                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Amplifier \n");
                                } else if (defaulAmResponseMessage.getAlarmInfo().getFiltersAlarm()) {
                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Filters \n");
                                } else {
                                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Power Supply \n");
                                }
                            }
                            case RemoteControlError -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: remote Control Error \n");
                            }
                            case TypingError -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: typing Error \n");
                            }
                            case LocalControl -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: local Control \n");
                            }
                            case UNRECOGNIZED -> {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: unknown request \n");
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        LogFPS.appendText(" received: error Set level 25% \n");
                    }

                    @Override
                    public void onCompleted() {

                    }
                });

            }
        }

    }
    //Кнопка запроса отказов
    public void ClickButtonRequestRefuses (ActionEvent actionEvent) {

        clientJSG.asyncStub.errorsRequestAmp(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.FailureAmResponseMessage>() {
            List<Byte> ErrorsList = new ArrayList<>();

            @Override
            public void onNext(JSG.FailureAmResponseMessage failureAmResponseMessage) {

                for (var iter : failureAmResponseMessage.getFailureBitsNums())
                    ErrorsList.add(iter);

                ucDescriptionsTable.setList(ErrorsList);
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onCompleted() {

            }
        });
    }

    //Кнопка запроса состояния УМ
    public void ClickStatusRequestAmplifier(ActionEvent actionEvent) {
        FPSModel model = new FPSModel();
        clientJSG.asyncStub.statusRequestAmp(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.StateAmResponseMessage>() {
            @Override
            public void onNext(JSG.StateAmResponseMessage stateAmResponseMessage) {
                var errorCode = stateAmResponseMessage.getJsgError();
                errorCode.getCommandCode();
                var errors = errorCode.getErrors();
                StringBuilder temp = new StringBuilder();
                for (int i = 0; i < errors.size(); i++) {
                    temp.append(errors.toByteArray()[i]);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            if (temp.isEmpty()) {
                                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: " + temp + "\n");
                            }
                        }
                    });
                }


                if (stateAmResponseMessage.getHasFailure()) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error Amplifier \n");
                        }
                    });
                }

                model.setPower(stateAmResponseMessage.getPower());
                model.setKsv(stateAmResponseMessage.getVSWR());
                model.setSupply(stateAmResponseMessage.getHasPower());
                model.setFilter(stateAmResponseMessage.getSetted2Filter());
                model.setRadiation(stateAmResponseMessage.getHasRadiation());
                model.setTemperature(stateAmResponseMessage.getTemperature());
                model.setControl(stateAmResponseMessage.getIsLocalControl());
                ucParamsTable.listParamFPS.clear();
                ucParamsTable.listParamFPS.add(model);
                ucParamsTable.UpdateStatus(ucParamsTable.listParamFPS);

                if (stateAmResponseMessage.getHasPower()) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            bOnOffPower.setText("Откл. питание");
                            PowerSupply = true;
                        }
                    });
                } else {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            bOnOffPower.setText("Вкл. питание");
                            PowerSupply = false;
                        }
                    });
                }

                if (stateAmResponseMessage.getHasRadiation()) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            bOnOffRadiation.setText("Откл. изл.");
                            RadiationStatus = true;
                        }
                    });
                } else {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            bOnOffRadiation.setText("Вкл. изл.");
                            RadiationStatus = false;
                        }
                    });
                }


                if (stateAmResponseMessage.getIsReady()) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: amplifier is ready \n");
                        }
                    });
                }
            }


            @Override
            public void onError(Throwable throwable) {
                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: error \n");
            }

            @Override
            public void onCompleted() {

            }
        });

    }

    // установить соединение с  сервером
    public void ClickConnect(ActionEvent actionEvent) throws IOException {
        LogFPS.appendText(simpleDateFormat.format(new Date()) + " sent: connect to Server" + "\n");
        clientJSG = new GrpcClientModel("127.0.0.1", 8888);
        clientJSG.asyncStub.ping(JSG.DefaultRequest.getDefaultInstance(), new StreamObserver<JSG.DefaultResponse>() {

            @Override
            public void onNext(JSG.DefaultResponse defaultResponse) {
                if (defaultResponse.getIsSuccessed()) {
                    ServerConnectionsState = ConnectionsStates.Connected;
                    ButtonServerColor(ServerConnectionsState, bConnect);
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: server connected " + "\n");
                } else {
                    ServerConnectionsState = ConnectionsStates.Disconnected;
                    ButtonServerColor(ServerConnectionsState, bConnect);
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: server is not connected " + "\n");
                }
            }

            @Override
            public void onError(Throwable throwable) {
                ServerConnectionsState = ConnectionsStates.Disconnected;
                ButtonServerColor(ServerConnectionsState, bConnect);
                LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: server is not connected " + "\n");
            }

            @Override
            public void onCompleted() {

            }
        });

    }

    // установить соединение с  ФПС
    public void ClickConnectJSG(ActionEvent actionEvent) {
        LogFPS.appendText(simpleDateFormat.format(new Date()) + " sent: connect to JSG" + "\n");

        clientJSG.asyncStub.connectToJSG(JSG.IPAddressMessage.newBuilder().setIP("192.168.0.114").setPort(9114).build(), new StreamObserver<JSG.DefaultResponse>() {
            @Override
            public void onNext(JSG.DefaultResponse defaultResponse) {
                if (defaultResponse.getIsSuccessed()) {
                    ServerConnectionsState = ConnectionsStates.Connected;
                    ButtonServerColor(ServerConnectionsState, bConnectJSG);
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: JSG connected " + "\n");
                } else {
                    ServerConnectionsState = ConnectionsStates.Disconnected;
                    ButtonServerColor(ServerConnectionsState, bConnectJSG);
                    LogFPS.appendText(simpleDateFormat.format(new Date()) + " received: JSG is not connected " + "\n");
                }
            }

            @Override
            public void onError(Throwable throwable) {
                ServerConnectionsState = ConnectionsStates.Disconnected;
                ButtonServerColor(ServerConnectionsState, bConnectJSG);
            }

            @Override
            public void onCompleted() {

            }
        });


    }

    // Цвета для кнопок
    private void ButtonServerColor(ConnectionsStates state, Button button) {
        switch (state) {
            case Connected:
                button.setStyle("-fx-background-color: green; -fx-border-color: grey;-fx-background-insets: 1.5,1.5,1.5,1;");
                break;
            case Disconnected:
                button.setStyle("-fx-background-color: red; -fx-border-color: grey;-fx-background-insets: 1.5,1.5,1.5,1;");
                break;
            case Unknown:
                button.setStyle("-fx-background-color: #292929; -fx-border-color: grey;-fx-background-insets: 1.5,1.5,1.5,1;");
        }

    }

    private void SetTooltip() {
        Tooltip tooltip = new Tooltip();

        ImageView image = new ImageView();
        image = new ImageView(new Image(FPSControlDebug.class.getResourceAsStream("Resources/Cad.gif"), 290.0D, 207.0D, true, true));
        tooltip.setGraphic(image);
        this.bOnOffRadiation.setTooltip(tooltip);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        ucSuppressFWS.addListener(this);
        ucSuppressFHSS.addListener(this);

        power_percentage.getItems().setAll(new String[]{"100", "50", "25"});
        power_percentage.setValue("100");

        fft.getItems().setAll(new Integer[]{12208, 6104, 3052});
        fft.setValue(6104);

        SetTooltip();

    }

    @Override
    public void OnAddRecord(TableSuppressFWSEvents tableSuppressFWSEvents) {
        List<SuppressFWSModel> list = new ArrayList<>();
        list.addAll(ucSuppressFWS.tableSuppressFWS.getItems().stream().toList());
        list.add(tableSuppressFWSEvents.getRecord());
        ucSuppressFWS.UpdateSuppressFWS(list);

    }

    @Override
    public void OnChangeRecord(TableSuppressFWSEvents tableSuppressFWSEvents) {
        List<SuppressFWSModel> list = new ArrayList<>();
//        list.addAll(ucSuppressFWS.tableSuppressFWS.getItems().stream().toList());
        list.clear();
        list.add(tableSuppressFWSEvents.getRecord());
        ucSuppressFWS.UpdateSuppressFWS(list);









    }

    @Override
    public void OnDeleteRecord(TableSuppressFWSEvents tableSuppressFWSEvents) {

    }

    @Override
    public void OnAddRecord(TableSuppressFHSSEvents tableSuppressFHSSEvents) {

        List<SuppressFHSSModel> list = new ArrayList<>();
        list.addAll(ucSuppressFHSS.tableSuppressFHSS.getItems().stream().toList());
        list.add(tableSuppressFHSSEvents.getRecord());
        ucSuppressFHSS.UpdateSuppressFHSS(list);


//        List<SuppressFHSSModel> list = new ArrayList<>();
//        list.add(tableSuppressFHSSEvents.getRecord());
//        ucSuppressFHSS.UpdateSuppressFHSS(list);
    }

    @Override
    public void OnChangeRecord(TableSuppressFHSSEvents tableSuppressFHSSEvents) {

    }

    @Override
    public void OnDeleteRecord(TableSuppressFHSSEvents tableSuppressFHSSEvents) {

    }

    @Override
    public void OnClearRecords(String s) {

        switch (s) {

            case "TableSuppressFWS":
                break;

            case "TableSuppressFHSS":
                int i = 90;
                break;
        }

    }

}


