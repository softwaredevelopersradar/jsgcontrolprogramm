package SuppressFWSControl;

import FPSModels.SuppressFWSModel;
import FPSModels.InterferenceParams;
import FPSValuesLib.PropertiesValues;
import FPSValuesLib.Suppress.InitInterferenceParams;
import FPSValuesLib.Translation.CMeaning;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class WndPropertySuppressFWS extends AnchorPane implements Initializable {

    public static Stage WindowProperty;
    static Scene FrontEndProperty;

    public int idField;
    public int getIdField() { return idField; }
    public void setIdField(int idField) { this.idField = idField; }

    public int numberASP;
    public boolean isCheckField;
    @FXML public Spinner<Integer> freqField;
    @FXML public ComboBox<String> modulationField;
    @FXML public ComboBox<String> deviationField;
    @FXML public ComboBox<String> manipulationField;
    @FXML public Spinner<Integer> freqSpacingField;
    @FXML public ComboBox<String> durationField;

    public SuppressFWSModel suppressFWSModel = new SuppressFWSModel();

    public boolean IsAddRec = false;
    public boolean IsChangeRec = false;

    private List<IPropertySuppressFWSEvents> listenersProperty = new ArrayList<IPropertySuppressFWSEvents>();

    public void addListenerProperty(IPropertySuppressFWSEvents toAdd) {
        listenersProperty.add(toAdd);
    }


    public WndPropertySuppressFWS() {

        InitProperty();

        WindowProperty.setTitle(CMeaning.getMeaningAddRecord()); //"Добавить запись"
        WindowProperty.getIcons().add(new Image(getClass().getResourceAsStream("IcoAddRec.png")));
    }

    public WndPropertySuppressFWS(SuppressFWSModel model) {

        InitProperty();

        WindowProperty.setTitle(CMeaning.getMeaningChangeRecord()); //"Изменить запись"
        WindowProperty.getIcons().add(new Image(getClass().getResourceAsStream("IcoChangeRec.png")));
    }

    private void InitProperty() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("WndPropertySuppressFWS.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            FrontEndProperty = new Scene(fxmlLoader.load(), 289, 297);
            WindowProperty = new Stage();
            WindowProperty.initModality(Modality.APPLICATION_MODAL);
            WindowProperty.setResizable(false);
            WindowProperty.setScene(FrontEndProperty);
            WindowProperty.show();

        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    private void Yes_Click(ActionEvent event) {

        suppressFWSModel.setId(getIdField());

        InterferenceParams interferenceParams = InitInterferenceParams.GetInterferenceParams(
                (modulationField.getSelectionModel().getSelectedIndex()),
                (deviationField.getSelectionModel().getSelectedIndex()),
                (manipulationField.getSelectionModel().getSelectedIndex()),
                (durationField.getSelectionModel().getSelectedIndex()),
                Integer.parseInt(String.valueOf(freqSpacingField.getValue())) / 20);

        suppressFWSModel.setInterferenceParams(interferenceParams);

        suppressFWSModel.setFrequency(freqField.getValue());

        if(IsAddRec) {

            suppressFWSModel.setIsCheck(true);

            for (IPropertySuppressFWSEvents propertyEvent : listenersProperty)
                propertyEvent.OnIsAddRecord(true);

            WindowProperty.close();
            IsAddRec = false;
        }

        if(IsChangeRec) {

            suppressFWSModel.setIsCheck(isCheckField);

            for (IPropertySuppressFWSEvents propertyEvent : listenersProperty)
                propertyEvent.OnIsChangeRecord(true);

            WindowProperty.close();
            IsChangeRec = false;
        }

    }

    @FXML
    private void MouseClickedModulation_Click(ActionEvent event) throws FileNotFoundException {

        PropertiesValues.setIndexModulation(Byte.parseByte(String.valueOf(modulationField.getSelectionModel().getSelectedIndex())));

        byte indexModulation = PropertiesValues.getIndexModulation();

        SetToolTipModulation(PropertiesValues.getIndexModulation());

        InitInterferenceParams.SetManipulation(manipulationField, PropertiesValues.getIndexModulation());
        InitInterferenceParams.SetDeviation(deviationField, PropertiesValues.getIndexModulation());

        // true - Disable, false - Enable
        if(indexModulation < 2 | indexModulation == 8) {

            deviationField.setDisable(false);
            manipulationField.setDisable(true);
            freqSpacingField.setDisable(true);
        }

        if(indexModulation > 1 & indexModulation < 5) {

            deviationField.setDisable(true);
            manipulationField.setDisable(false);
            freqSpacingField.setDisable(false);

            CalculateSpectrumWidth();
        }

        if(indexModulation > 4 & indexModulation < 8 | indexModulation == 9) {

            deviationField.setDisable(false);
            manipulationField.setDisable(false);
            freqSpacingField.setDisable(true);
        }
    }

    @FXML
    private void MouseClickedManipulation_Click(ActionEvent event) throws FileNotFoundException {

        PropertiesValues.setIndexModulation(Byte.parseByte(String.valueOf(modulationField.getSelectionModel().getSelectedIndex())));

        CalculateSpectrumWidth();
    }

    @FXML
    private void MouseClickedFreqSpacing_Click(MouseEvent event) throws FileNotFoundException {

        PropertiesValues.setIndexModulation(Byte.parseByte(String.valueOf(modulationField.getSelectionModel().getSelectedIndex())));

        CalculateSpectrumWidth();
    }

    /**
     * Рассчет ширины спектра
     */
    private void CalculateSpectrumWidth() {

        try{

            double spectrumWidth = 0;
            boolean IsSpectrumWidth = false;

            switch (PropertiesValues.getIndexModulation()) {

                case 2: // ЧМ-2

                    spectrumWidth = (freqSpacingField.getValue() + Double.parseDouble(String.valueOf(manipulationField.getValue()))) / 1000;
                    IsSpectrumWidth = true;
                    break;

                case 3: // ЧМ-4

                    spectrumWidth = (3 * freqSpacingField.getValue() + Double.parseDouble(String.valueOf(manipulationField.getValue()))) / 1000;
                    IsSpectrumWidth = true;
                    break;

                case 4: // ЧМ-8

                    spectrumWidth = (7 * freqSpacingField.getValue() + Double.parseDouble(String.valueOf(manipulationField.getValue()))) / 1000;
                    IsSpectrumWidth = true;
                    break;

                default:
                    break;
            }

            if(IsSpectrumWidth) {

                deviationField.getItems().clear();
                deviationField.getItems().add(String.valueOf(spectrumWidth));
                deviationField.getSelectionModel().select(0);
            } else {

                if (PropertiesValues.getIndexModulation() < 2 | PropertiesValues.getIndexModulation() == 8) {
                    this.deviationField.setDisable(false);
                    this.manipulationField.setDisable(true);
                    this.freqSpacingField.setDisable(true);
                }
            }
        }
        catch (Exception ex){

        }
    }

    /**
     * Set ToolTip for ComboBox Modulation
     * @param indexModulation index of item
     */
    private void SetToolTipModulation(byte indexModulation){

        Tooltip tooltip = new Tooltip();
        modulationField.setTooltip(tooltip);
        ImageView image = new ImageView();

        switch (PropertiesValues.getIndexModulation()){

            case 0: // ЧМШ
            case 1: // ЧМШ скан. f

                image = new ImageView(new Image(WndPropertySuppressFWS.class.getResourceAsStream("Resources/ChMSh.gif"), 273, 166, true, true));
                break;

            case 2: // ЧМ-2

                image = new ImageView(new Image(WndPropertySuppressFWS.class.getResourceAsStream("Resources/ChM2.gif"), 267, 196, true, true));
                break;

            case 3: // ЧМ-4

                image = new ImageView(new Image(WndPropertySuppressFWS.class.getResourceAsStream("Resources/ChM4.gif"), 265, 196, true, true));
                break;

            case 4: // ЧМ-8

                image = new ImageView(new Image(WndPropertySuppressFWS.class.getResourceAsStream("Resources/ChM8.gif"), 290, 207, true, true));
                break;

            case 5: // ФМ-2
            case 6: // ФМ-4
            case 7: // ФМ-8
            case 8: // ЗШ (КФМ)
            case 9: // АФМн

                image = new ImageView(new Image(WndPropertySuppressFWS.class.getResourceAsStream("Resources/FM2.gif"), 316, 207, true, true));
                break;

            default:
                break;
        }

        tooltip.setGraphic(image);
        modulationField.setTooltip(tooltip);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        isCheckField = true;
        freqField.getValueFactory().setValue(1500000);
        freqSpacingField.getValueFactory().setValue(20);

        InitInterferenceParams.InitModulation(modulationField);
        InitInterferenceParams.InitDuration(durationField);
        InitInterferenceParams.InitManipulation(manipulationField, (byte)5);
        InitInterferenceParams.InitDeviation(deviationField, 8);

    }

}
