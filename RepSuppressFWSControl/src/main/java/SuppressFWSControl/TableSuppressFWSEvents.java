package SuppressFWSControl;

import FPSModels.SuppressFWSModel;

public class TableSuppressFWSEvents {

    private SuppressFWSModel record;
    private Integer id;

    public TableSuppressFWSEvents(SuppressFWSModel record)
    {
        this.record = record;
        this.id = record.getId();
    }

    public SuppressFWSModel getRecord() { return record; }

    public Integer getId() { return id; }
}
