package SuppressFWSControl;

public interface IPropertySuppressFWSEvents {

    public void OnIsAddRecord(Boolean b);
    public void OnIsChangeRecord(Boolean b);

}
