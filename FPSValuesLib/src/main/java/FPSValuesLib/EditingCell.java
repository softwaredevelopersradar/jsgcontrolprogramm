package FPSValuesLib;

import FPSModels.SuppressFWSModel;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;

class EditingCell extends TableCell<SuppressFWSModel, Boolean> {
    private Boolean textField;

    public EditingCell() {
    }

    public void startEdit() {
        super.startEdit();
        if (this.textField == null) {
            this.createTextField();
        }

    }

    public void cancelEdit() {
        super.cancelEdit();
        this.setText(String.valueOf(this.getItem()));
        this.setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    public void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            this.setText((String)null);
        } else if (this.isEditing()) {
            if (this.textField != null) {
            }

            this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        } else {
            this.setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

    }

    private void createTextField() {
    }

    private Boolean getBoolean() {
        return (Boolean)this.getItem() ? false : (Boolean)this.getItem();
    }
}
