package FPSValuesLib.Translation;

public class CMeaning {

    // Значения
    public static String meaningAddRecord = "Добавить запись";
    public static String meaningChangeRecord = "Изменить запись";

    public static String meaningOwn = "Ведущая";
    public static String meaningLinked = "Ведомая";
    public static String meaningAutonomous = "Автономная";

    public static String meaningHz = "Гц";
    public static String meaningkHz = "кГц";
    public static String meaningMHz = "МГц";

    public static String meaningmks = "мкс";
    public static String meaningms = "мс";

    public static String getMeaningAddRecord() { return meaningAddRecord; }
    public static void setMeaningAddRecord(String meaningAddRecord) { CMeaning.meaningAddRecord = meaningAddRecord; }

    public static String getMeaningChangeRecord() { return meaningChangeRecord; }
    public static void setMeaningChangeRecord(String meaningChangeRecord) { CMeaning.meaningChangeRecord = meaningChangeRecord; }

    public static String getMeaningOwn() { return meaningOwn; }
    public static void setMeaningOwn(String meaningOwn) { CMeaning.meaningOwn = meaningOwn; }

    public static String getMeaningLinked() { return meaningLinked; }
    public static void setMeaningLinked(String meaningLinked) { CMeaning.meaningLinked = meaningLinked; }

    public static String getMeaningAutonomous() { return meaningAutonomous; }
    public static void setMeaningAutonomous(String meaningAutonomous) { CMeaning.meaningAutonomous = meaningAutonomous; }

    public static String getMeaningHz() { return meaningHz; }
    public static void setMeaningHz(String meaningHz) { CMeaning.meaningHz = meaningHz; }

    public static String getMeaningkHz() { return meaningkHz; }
    public static void setMeaningkHz(String meaningkHz) { CMeaning.meaningkHz = meaningkHz; }

    public static String getMeaningMHz() { return meaningMHz; }
    public static void setMeaningMHz(String meaningMHz) { CMeaning.meaningMHz = meaningMHz; }

    public static String getMeaningmks() { return meaningmks; }
    public static void setMeaningmks(String meaningmks) { CMeaning.meaningmks = meaningmks; }

    public static String getMeaningms() { return meaningms; }
    public static void setMeaningms(String meaningms) { CMeaning.meaningms = meaningms; }
}
