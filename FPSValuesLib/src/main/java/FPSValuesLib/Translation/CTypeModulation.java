package FPSValuesLib.Translation;

public class CTypeModulation {

    // Вид модуляции
    public static String paramChMSh = "ЧМШ";
    public static String paramChMShScan = "ЧМШ скан. f";
    public static String paramChM2= "ЧМ-2";
    public static String paramChM4= "ЧМ-4";
    public static String paramChM8= "ЧМ-8";
    public static String paramFM2 = "ФМ-2";
    public static String paramFM4 = "ФМ-4";
    public static String paramFM8 = "ФМ-8";
    public static String paramZSh = "ЗШ";
    public static String paramAFMn = "АФМн";

    public static String getParamChMSh() { return paramChMSh; }
    public static void setParamChMSh(String paramChMSh) { CTypeModulation.paramChMSh = paramChMSh; }

    public static String getParamChMShScan() { return paramChMShScan; }
    public static void setParamChMShScan(String paramChMShScan) { CTypeModulation.paramChMShScan = paramChMShScan; }

    public static String getParamChM2() { return paramChM2; }
    public static void setParamChM2(String paramChM2) { CTypeModulation.paramChM2 = paramChM2; }

    public static String getParamChM4() { return paramChM4; }
    public static void setParamChM4(String paramChM4) { CTypeModulation.paramChM4 = paramChM4; }

    public static String getParamChM8() { return paramChM8; }
    public static void setParamChM8(String paramChM8) { CTypeModulation.paramChM8 = paramChM8; }

    public static String getParamFM2() { return paramFM2; }
    public static void setParamFM2(String paramFM2) { CTypeModulation.paramFM2 = paramFM2; }

    public static String getParamFM4() { return paramFM4; }
    public static void setParamFM4(String paramFM4) { CTypeModulation.paramFM4 = paramFM4; }

    public static String getParamFM8() { return paramFM8; }
    public static void setParamFM8(String paramFM8) { CTypeModulation.paramFM8 = paramFM8; }

    public static String getParamZSh() { return paramZSh; }
    public static void setParamZSh(String paramZSh) { CTypeModulation.paramZSh = paramZSh; }

    public static String getParamAFMn() { return paramAFMn; }
    public static void setParamAFMn(String paramAFMn) { CTypeModulation.paramAFMn = paramAFMn; }
}
