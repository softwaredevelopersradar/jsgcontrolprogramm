package FPSTableDiscriptionsControl;

import FPSModels.ErrorsModel;
import FPSValuesLib.Converters;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class TableDescriptions extends AnchorPane implements Initializable {

    @FXML public TableColumn<ErrorsModel, Date> timeColumn;
    @FXML public TableColumn<ErrorsModel, Byte> descriptionColumn;
    @FXML public TableView<ErrorsModel> tableDescription;

    public TableDescriptions() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TableDescriptions.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            fxmlLoader.load();

        }
        catch (IOException exception) {

            throw new RuntimeException(exception);
        }
    }

    private List<Byte> listBytes;
    public List<Byte> getList() {
        return listBytes;
    }

    public void setList(List<Byte> listBytes) {
        this.listBytes = listBytes;
        UpdateError();
    }

    private void UpdateError() {

        List<ErrorsModel> listErrorsModel = new ArrayList<ErrorsModel>() ;

        for (int i = 0; i < listBytes.stream().count(); i++) {

            ErrorsModel model = new ErrorsModel(listBytes.get(i));
            listErrorsModel.add(model);
        }

        ObservableList<ErrorsModel> tempList = FXCollections.observableArrayList(listErrorsModel);
         tableDescription.setItems(tempList);
    }





    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        this.timeColumn.setCellValueFactory(new PropertyValueFactory("time"));
        this.descriptionColumn.setCellValueFactory(new PropertyValueFactory("description"));

        descriptionColumn.setCellFactory(new Callback<TableColumn<ErrorsModel, Byte>, TableCell<ErrorsModel, Byte>>() {

            @Override
            public TableCell<ErrorsModel, Byte> call(TableColumn<ErrorsModel, Byte> param) {
                return new TableCell<ErrorsModel, Byte>() {

                    private final javafx.scene.control.Label label;

                    {
                        label = new Label();
                        label.setStyle("-fx-text-fill: white;");
                        label.setTooltip(new Tooltip("Описание отказа"));

                    }

                    @Override
                    protected void updateItem(Byte value, boolean empty) {
                        super.updateItem(value, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            ////////////////////////////
                            label.setText(Converters.ErrorsConverter(value));
                            setGraphic(label);
                            /////////////////////////////
                        }

                    }

                };
            }
        });

        this.timeColumn.setCellFactory(new Callback<TableColumn<ErrorsModel, Date>, TableCell<ErrorsModel, Date>>() {
            public TableCell<ErrorsModel, Date> call(TableColumn<ErrorsModel, Date> param) {
                return new TableCell<ErrorsModel, Date>() {
                    private final Label label = new Label();
                    {
                        this.label.setStyle("-fx-text-fill: white;");
                        this.label.setTooltip(new Tooltip("Время появления отказа"));
                    }
                    protected void updateItem(Date value, boolean empty) {
                        super.updateItem(value, empty);
                        if (empty) {
                            this.setGraphic((Node)null);
                        } else {
                            SimpleDateFormat formatForDateNow = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy ");
                            this.label.setText(formatForDateNow.format(value));
                            this.setGraphic(this.label);
                        }
                    }
                };
            }
        });
    }
}
